#pragma once

#include <string>
#include <stdio.h>
#include <windows.h>
using namespace std;

//zapne/vypne v�pis (nalezen� moduly, fce a jejich addr - p�i ka�d�m vol�n� nahrady)
#define UKECANE false
#define DLL_NAME "ucrtbased.dll"


//struktura pro ulo�en� alokace
struct debugAlloc {
	void* pointer = nullptr;
	size_t size = 0;
};

/**
 * Slou�� k dynamick�mu alokov�n� pole pro z�znamy alokac�
**/
void doalokujPole();

/**
 * Nahrad� ukazatel na funkci name v import table, vrac� p�vodn� adresu
**/
DWORD nahrada(string dllName, string name, DWORD newFunction);

/**
 * podstr�� moje funkce pro alokace
**/
void MallocDebug_Init();

/**
 * vr�t� zp�t puvodni fce
**/
void MallocDebug_Done();

//fce pro podstrceni
void* my_malloc(size_t size);
void* my_calloc(size_t num, size_t size);
void* my_realloc(void* ptr, size_t size);
void  my_free(void* ptr);