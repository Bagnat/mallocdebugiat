#include <stdio.h>
#include <windows.h>
#include <string>
#include <iostream>
#include <vector>
#include "debug.h"
using namespace std;

int main(int argc, char** argv)
{

	{
		char* buffer1 = (char*)malloc(10);		//neuvolnim, ale alokoju pred Initem
		char* buffer11 = (char*)malloc(10);		//uvolnim po initu, ale alokuji pred Initem

		MallocDebug_Init();

		char* buffer2 = (char*)malloc(10);
		buffer2 = (char*)realloc(buffer2, 20);
		buffer2 = (char*)realloc(buffer2, 40);	//neuvolnim, ocekavam spravnou alokaci

		char* buffer3 = (char*)malloc(-1);		//necekam ze se naalokuje
		if (buffer3 != nullptr)
			cout << "buffer3 se naalokoval s -1??" << endl;

		uint8_t* poleCisel = (uint8_t*)calloc(213, 1);			//zkouska callocu bez uvolneni
		uint8_t* poleCiselFreed = (uint8_t*)calloc(213, 1);		//zkouska callocu s uvolnenim

		//rychle otestování zda se hodnoty vymazaly a pole naalokovaly
		if (poleCisel == nullptr || poleCiselFreed == nullptr) cout << "poleCisel se melo naalokovat" << endl;
		else
		{
			for (uint8_t i = 0; i < 213; i++) {
				if (poleCisel[i] != 0 || poleCiselFreed[i] != 0) cout << "asi se calloc nezavolal" << endl;
			}
		}

		cout << "# ocekavane nezname uvolneni: buffer11" << endl;
		cout << "# ocekavane neuvolneni: buffer2 (40B), poleCisel (213B)" << endl << endl;

		free(poleCiselFreed);
		free(buffer11);


		MallocDebug_Done();
	}


	{
		cout << endl << "# alokace 2d pole - bez uvolneni" << endl;
		cout << "# ocekavam neuvolnenych 101 prvku" << endl << endl;

		MallocDebug_Init();

		double** Matice = (double**)malloc(sizeof(double*) * 100);
		for (int i = 0; i < 100; i++) {
			Matice[i] = (double*)malloc(sizeof(double) * (i + 1) * 100);
		}
		Matice[2][71] = 2.71;

		MallocDebug_Done();
	}



	{
		cout << endl << "# alokace 2d pole - s uvolnenim krom 50teho prvni ( 8*(50+1)*100 = 40800B )" << endl << endl;

		MallocDebug_Init();

		double** Matice = (double**)malloc(sizeof(double*) * 100);
		for (int i = 0; i < 100; i++) {
			Matice[i] = (double*)malloc(sizeof(double) * (i + 1) * 100);
		}

		Matice[3][14] = 3.14;

		for (int i = 99; i > 50; i--) {
			free(Matice[i]);
		}

		for (int i = 0; i < 50; i++) {
			free(Matice[i]);
		}
		free(Matice);

		MallocDebug_Done();
	}


	{
		cout << endl << "# realokace neznameho prvku" << endl << endl;
		float* vector = (float*)malloc(8 * 4);
		MallocDebug_Init();
		vector = (float*)realloc(vector, 16 * 4);
		free(vector);
		MallocDebug_Done();
	}

	{
		cout << endl << "# testovani malloc(0)" << endl;
		cout << "# jednou v poradku, jednou alokuji ale neuvolnim a jednou nealokuji, ale uvolnim" << endl << endl;

		MallocDebug_Init();
		char* p = (char*)malloc(0);
		free(p);
		MallocDebug_Done();

		MallocDebug_Init();
		p = (char*)malloc(0);
		MallocDebug_Done();
		free(p);

		p = (char*)malloc(0);
		MallocDebug_Init();
		free(p);
		MallocDebug_Done();

	}


	{
		cout << endl << "# testovani jine pouziti reallocu nez klasicke" << endl;
		MallocDebug_Init();
		char* p = (char*)realloc(nullptr, 50);
		free(p);
		char* q = (char*)realloc(nullptr, 50);
		q = (char*)realloc(q, 0);
		MallocDebug_Done();
	}

	{
		cout << endl << "# test realloc a free" << endl;
		char* p = (char* ) malloc(1);
		MallocDebug_Init();
		char* q = (char* ) realloc(p, 2);
		free(q);
		MallocDebug_Done();
	}

	return 0;
}