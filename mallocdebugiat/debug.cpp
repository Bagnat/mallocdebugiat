#include "debug.h"
#include <iostream>

//Glob�ln� prom�nn�
struct debugAlloc* AllocField;		//statick� pole, kam budu ukladat info
UINT debugAllocCount = 0;			//po�et zaznamenan�ch alokaci
UINT debugAllocAlloc = 0;			//po�et p�edalokovan�ch pol� pro ulozeni
BOOL debugEnable = false;			//pozastaveni/spusten� logov�n�

//Ukazatel� na fce
void* (*ADDR_malloc) (size_t);
void* (*ADDR_calloc) (size_t, size_t);
void* (*ADDR_realloc) (void*, size_t);
void  (*ADDR_free) (void*);


DWORD nahrada(string dllName, string name, DWORD newFunction) {

	HMODULE hPEFile = GetModuleHandle(NULL); // NULL means the current process
	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)hPEFile;

	PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((BYTE*)pDosHeader) + pDosHeader->e_lfanew);

	if (UKECANE) printf("Dos header addr: %p\n", pDosHeader);
	if (UKECANE) printf("PIMAGE_NT_HEADERS : %p\n", pNTHeaders);

	if (pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size == 0) return NULL;
	if (pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress == NULL) return NULL;

	//prvni import desc.
	PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor =
		(PIMAGE_IMPORT_DESCRIPTOR)(((BYTE*)pDosHeader) + pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

	//posledni
	PIMAGE_IMPORT_DESCRIPTOR pImportDescriptorEnd = (PIMAGE_IMPORT_DESCRIPTOR)(((BYTE*)pImportDescriptor) + pNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size);


	DWORD return_value = NULL;
	while (pImportDescriptor < pImportDescriptorEnd && pImportDescriptor->Characteristics != NULL ) {

		char* current_dllName = (char*)(((BYTE*)pDosHeader) + pImportDescriptor->Name);
		if (dllName.compare(current_dllName) != 0) {
			pImportDescriptor++;
			continue;
		}
		if (UKECANE) printf("DDL name: %s\n", current_dllName);

		//original thunk 
		PIMAGE_THUNK_DATA origFirstThunk = (PIMAGE_THUNK_DATA)(((BYTE*)pDosHeader) + pImportDescriptor->OriginalFirstThunk);
		PIMAGE_THUNK_DATA firstThunk = (PIMAGE_THUNK_DATA)(((BYTE*)pDosHeader) + pImportDescriptor->FirstThunk);


		while (true) {
			auto image_thunk = origFirstThunk->u1;
			auto import_table = firstThunk->u1;

			if (image_thunk.AddressOfData == NULL) break;

			PIMAGE_IMPORT_BY_NAME image_imp_name = (PIMAGE_IMPORT_BY_NAME)(((BYTE*)pDosHeader) + image_thunk.AddressOfData);

			if (UKECANE) printf("\tfce: %s - %p\n", image_imp_name->Name, (void*)import_table.Function);

			if (name.compare(image_imp_name->Name) == 0) {
				if (return_value != NULL && return_value != import_table.Function) printf("Byly nahrazeny 2 funkce a ka�d� m� jinou adresu!");
				return_value = import_table.Function;

				DWORD oldPriv = 0;
				VirtualProtect(&(firstThunk->u1.Function), 4, PAGE_READWRITE, &oldPriv);
				firstThunk->u1.Function = (DWORD)newFunction;
				VirtualProtect(&(firstThunk->u1.Function), 4, oldPriv, &oldPriv);

			}

			origFirstThunk++;
			firstThunk++;
		}

		pImportDescriptor++;
	}

	return return_value;
}

void doalokujPole() {
	if (debugAllocAlloc == 0) {
		debugAllocAlloc = 10;
		AllocField = (struct debugAlloc*) ADDR_malloc(debugAllocAlloc * sizeof(struct debugAlloc));
	}
	else {
		debugAllocAlloc *= 2;
		AllocField = (struct debugAlloc*) ADDR_realloc(AllocField, debugAllocAlloc * sizeof(struct debugAlloc));
	}
	if (AllocField == nullptr) cout << "nastala chyba alokace: my_calloc" << endl;
}

void* my_malloc(size_t size) {
	//volani fce
	void* p = ADDR_malloc(size);

	//pokud je mo�n�.. tak ukladam
	if (debugEnable && p != nullptr)
	{
		if (debugAllocCount == debugAllocAlloc) doalokujPole();
		AllocField[debugAllocCount].size = size;
		AllocField[debugAllocCount].pointer = p;
		debugAllocCount++;
	}

	return p;
}

void* my_calloc(size_t num, size_t size) {
	//volani fce
	void* p = ADDR_calloc(num, size);

	//pokud je mo�n�.. tak ukladam
	if (debugEnable && p != nullptr)
	{
		if (debugAllocCount == debugAllocAlloc) doalokujPole();
		AllocField[debugAllocCount].size = num * size;
		AllocField[debugAllocCount].pointer = p;
		debugAllocCount++;
	}

	return p;
}

void* my_realloc(void* ptr, size_t size) {
	//volani fce
	void* p = ADDR_realloc(ptr, size);

	//pokud je mo�n�.. tak ukladam
	if (debugEnable && p != nullptr) {
		if (ptr != nullptr) {
			for (UINT i = 0; i < debugAllocCount; i++) {
				if (AllocField[i].pointer == ptr) {
					AllocField[i].size = size;
					AllocField[i].pointer = p;
					return p;
				}
			}
			printf("realokuje se neznamy ukazatel na %p o velikosti %d B\n" , ptr, size);
		}
		
		//malloc
		if (debugAllocCount == debugAllocAlloc) doalokujPole();
		AllocField[debugAllocCount].size = size;
		AllocField[debugAllocCount].pointer = p;
		debugAllocCount++;
	
		
	}
	else if (debugEnable && size == 0 && ptr != nullptr) {
			//free
			for (UINT i = 0; i < debugAllocCount; i++) {
				if (AllocField[i].pointer == ptr) {
					if (i != debugAllocCount - 1)
						memcpy(&(AllocField[i]), &(AllocField[debugAllocCount - 1]), sizeof(struct debugAlloc));
					debugAllocCount--;
					return p;
				}
			}
			printf("nezname uvolneni adresy %p\n", ptr);
		
	}

	return p;
}

void  my_free(void* ptr) {
	if (debugEnable && ptr != nullptr) {
		for (UINT i = 0; i < debugAllocCount; i++) {
			if (AllocField[i].pointer == ptr) {
				if (i != debugAllocCount - 1)
					memcpy(&(AllocField[i]), &(AllocField[ debugAllocCount-1 ]), sizeof(struct debugAlloc));
				debugAllocCount--;
				ADDR_free(ptr);
				return;
			}
		}
		printf("nezname uvolneni adresy %p\n", ptr);
	}else if(ptr == nullptr) printf("volan� free na nullptr %p\n", ptr);

	//volani fce
	ADDR_free(ptr);
}

void MallocDebug_Init() {
	if (debugEnable == true) return;
	
	ADDR_malloc		= (void* (*) (size_t))			nahrada(DLL_NAME, "malloc", (DWORD)&my_malloc);
	ADDR_calloc		= (void* (*) (size_t, size_t))	nahrada(DLL_NAME, "calloc", (DWORD)&my_calloc);
	ADDR_realloc	= (void* (*) (void*, size_t))	nahrada(DLL_NAME, "realloc", (DWORD)&my_realloc);
	ADDR_free		= (void (*) (void*))			nahrada(DLL_NAME, "free",	(DWORD)&my_free);

	debugEnable = true;
}

void MallocDebug_Done() {
	if (debugEnable == false) return;
	debugEnable = false;
	nahrada(DLL_NAME, "malloc", (DWORD)ADDR_malloc);
	nahrada(DLL_NAME, "calloc", (DWORD)ADDR_calloc);
	nahrada(DLL_NAME, "realloc",(DWORD)ADDR_realloc);
	nahrada(DLL_NAME, "free",	(DWORD)ADDR_free);
	
	//vypis zbytkov�ch alokaci, uvolneni log�
	if (debugAllocCount != 0)
	{
		for (UINT i = 0; i < debugAllocCount; i++) {
			printf("neuvolnene misto na adrese 0x%p, velikost %d\n", AllocField[i].pointer, AllocField[i].size);
		}
		debugAllocCount = 0;
	}

	if (AllocField != nullptr) {
		free(AllocField);
		AllocField = nullptr;
	}
	debugAllocAlloc = 0;
}